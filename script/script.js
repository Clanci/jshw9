// Опишіть, як можна створити новий HTML тег на сторінці.
// Створити за допомогою document.createElement(tag) та додати на сторінку за допомогою append або інших методів.

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Перший параметр - спеціальне слово, яке вказує куди треба вставляти створений елемент.

// Як можна видалити елемент зі сторінки?
// за допомогою node.remove()

// Pеалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.

function createList(array, parent = document.body) {
    const listContainer = document.createElement('ul');

    array.forEach(item => {
        const listItem = document.createElement('li');

        if (Array.isArray(item)) {
            listItem.appendChild(createList(item));
        } else {
            listItem.textContent = item;
        }

        listContainer.appendChild(listItem);
    });

    parent.appendChild(listContainer);
    return listContainer;
}

const arrayToDisplay = ["hello", "world", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const parentElement = document.getElementById('listContainer');

let seconds = 3;

function updateTimer() {
    const timerElement = document.getElementById('timer');
    timerElement.textContent = seconds;
    seconds--;

    if (seconds < 0) {
        clearInterval(timerInterval);
        clearPage();
    }
}

function clearPage() {
    document.body.innerHTML = '';
}

const timerInterval = setInterval(updateTimer, 1000);

createList(arrayToDisplay, parentElement);